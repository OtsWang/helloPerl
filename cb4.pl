#! /usr/bin/perl


use Switch;

%phone = ();
sub show() {
    print "----------------\n";
    while (($name, $phone) = each(%phone)) {
        print "name: $name     phone: $phone\n";
    }
    print "----------------\n";
}


$msg = "1. Add a phone number
2. Lookup a phone number
3. Delete a phone number
4. Print all phone number
5. Check if a person has phone number
6. Exit the program
";

for( ; ; ) {

    print $msg;

    chomp($choice=<STDIN>);

    switch($choice) {
        case 1      {
            print "Please input user name:";
            chomp($name=<STDIN>);
            print "Input phone num:";
            chomp($phone=<STDIN>);
            $phone{$name} = $phone;

        }
        case 2      {
            print "Please input user name:";
            chomp($name=<STDIN>);
            $phone = $phone{$name};
            print "$name\'s phone is $phone\n";

        }
        case 3      {
            print "Please input user name:";
            chomp($name=<STDIN>);
            delete $phone{$name};

        }
        case 4      {
            show();

        }
        case 5      {
            print "Please input user name:";
            chomp($name=<STDIN>);
            if(exists $phone{$name}) {
                print "$name has a phone\n";
            } else {
                print "$name don't has a phone\n";
            }


        }
        case 6      {
            goto END;

        }
        else      {
            print "other\n";
        }

    }

}

END: print "See you next time\n";


