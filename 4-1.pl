#! /usr/bin/perl
# associative arrays is called hash 

# two ways to create hash
%dog_owner = qw(ots doney smj toney);
# => is same as comma in purl, so it is ok for a array 
# @names = ("ots" => "smj");
%dog_owner_2 = (
    "ots" => "doney",
    "smj" => "toney"
);

print "the dog of ots is $dog_owner{ots}\n";
print "the dog of smj is $dog_owner_2{smj}\n";

# $dog_owner{ots} = "doney2";
$dog_owner{"ots"} = "doney2";
print "the dog of ots is $dog_owner{'ots'}\n";

@owners = keys %dog_owner;
foreach $owner (@owners) {
    print "$owner has a dog named $dog_owner{$owner}\n";
}

print "another way to loop hash\n";

while (($owner, $dog) = each (%dog_owner)) {
    print "$owner has a dog $dog\n";
}

print "delete key/value or delete value\n";
$dog_owner{"family"} = "roney";

while (($owner, $dog) = each (%dog_owner)) {
    print "$owner has a dog $dog\n";
}

print "delete value---------------\n";
delete $dog_owner{"family"};
while (($owner, $dog) = each (%dog_owner)) {
    print "$owner has a dog $dog\n";
}
$dog_owner{"family"} = "roney";

print "delete key-----------------\n";
undef $dog_owner{"family"};
while (($owner, $dog) = each (%dog_owner)) {
    print "$owner has a dog $dog\n";
}

if (defined $dog_owner{family}) {
    print "defined family\n";
}

if (exists $dog_owner{family}) {
    print "exists family\n";
}

while (($key, $value) = each (%ENV)) {
    print "$key = $value \n";
}

print "argv: @ARGV\n";

