#! /usr/bin/perl    

@names = ("ots", "smj");

print "@names\n";

push(@names, "mj");
unshift(@names, "o");

# another way to push or pop
# @names = ("o", @names, "mj");
# ($first, @names, $last) = @names;

print "@names\n";

$last = pop(@names);
$first = shift(@names);

print "last is $last, and first is $first\n, and now is @names\n";

# the function of splice
splice ( @names, 1, 1, "Smj");
splice @names, 0, 0, "OtsWang";

print "@names\n";

for ($i=0; $i<=$#names; $i++) {
    print "@names[$i]\n";
}

# the $count val 
for $item (@names) {
    print ++$count . " $item\n";
}

@nums = (1,3,5,7,0,2,4,6,8,9);
print "@nums\n";
@nums_sort = sort @nums;
@nums_rev = reverse @nums;
print "sort @nums_sort\n";
print "reverse @nums_rev\n";

@directions = qw(e w s n es en ws wn);
print "@directions\n";

$direction_nums = @directions;
#$direction_nums = scalar (@directions);
print "the length: $direction_nums \n";

