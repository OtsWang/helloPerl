#! /usr/bin/perl 
use feature "switch";

chomp($age = <STDIN>);

if ($age < 18) {
    print "too young\n";
} elsif ($age > 22) {
    print "old enough\n";
} else {
    print "just ok\n";
}

print "too young\n" if $age < 10;

unless($age > 18) {
    print "you are not adult\n";
}

given($age) {
    when(10) {
        print "too young\n";
    }
    when(5) {
        print "kidd\n";
    }
    default {
        print "ok\n";
    }
}

while($age<20) {
    print "grown 1 year\n";
    $age ++;
}

until($age>25) {
    print "grown to 25 by 1 year\n";
    $age ++;
}

# last / next / goto 


