#! /usr/bin/perl

my $aaa = "abc";
my $bbb = 123;

print $aaa.$bbb;
print "\n";
print $aaa.$bbb.45;
print "\n";
print $aaa.$bbb.45.00;
print "\n";

print $aaa + $bbb;
print "\n";
print "   $aaa" + $bbb;
print "\n";

print "-------\n";
print "1000 monkeys" + 5;
print "\n";
print length(123.45000);
print "\n";
print "work" / "time";
